<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Home extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('home_model');
	}

	public function index()
	{
		if(!$this->session->userdata('user_id')) {
			$this->load->view('login');
		} else {
			$userID = $this->session->userdata('user_id');
			$data = $this->home_model->getUserData($userID)[0];
			$this->load->view('home',$data);
		}
		
	}

	public function logout() {
		$this->session->unset_userdata('user_id');
		redirect('login');
	}

	public function ChangeEmail() {		
		$userID = $this->session->userdata('user_id');
		$email = $this->input->post('email');
		$updates = $this->home_model->updateEmail($email,$userID);
		if($updates == 1) {
			echo 'success';
		} else {
			echo 'failed';
		}
	}

	public function deleteAcc() {
		$userID = $this->session->userdata('user_id');
		$deleted = $this->home_model->deleteAccount($userID);
		if($deleted == 1) {
			redirect('login');
		} else {
			redirect('home');
		}
	}
}
?>