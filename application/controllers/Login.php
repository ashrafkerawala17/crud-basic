<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('login');
	}

	function checkuser() {
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$exist = $this->Login_model->user_exist($email,$pass);
		if($exist) {
				echo $this->session->set_userdata('user_id',$exist[0]['id']);
				echo 'success';
		} else {
			echo 'failure';
		}
	}

	function createUser() {		
		$email = $this->input->post('email');
		$username = $this->input->post('user');
		$pass = $this->input->post('pass');		
		$inserted = $this->Login_model->create_user($email,$pass,$username);
		if($inserted == 1) {				
			$exist = $this->Login_model->user_exist($email,$pass);		
			echo $this->session->set_userdata('user_id',$exist[0]['id']);
			echo 'success';
		} else {
			echo 'failed';
		}
	}

	function signUp() {
		$this->load->view('signup');
	}
}
?>
