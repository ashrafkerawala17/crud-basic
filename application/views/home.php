<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!Doctype html>
<html>
	<head>
		<title>Beast</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel='stylesheet' href='assets/css/bootstrap.min.css'>
		<link rel='stylesheet' href='assets/css/login.css'>
	</head>
	<body>
		<div class="container">
			<h1>Welcome <?php echo $username;?></h1>
			<p>Your email address is <span class="newMail"><?php echo $email;?></span></p>
			<input class="changename" name="email" type="email" required>
			<button class="changeButton btn btn-secondary" type="submit">Change Email</button>
			<p class="msg"></p>
			<a href="<?php echo base_url();?>home/logout"><button class="btn btn-warning">Logout</button></a>
			<a href="<?php echo base_url();?>home/deleteAcc"><button class="btn btn-danger">Delete Account</button></a>
		</div>
	</body>
	<script src="assets/js/jquery.js"></script>
    <script>
        $(document).ready(function() {            
			var base_url = "<?php echo base_url();?>";
            $('.changeButton').click(function(){
				var email = $('.changename').val();
				if(email == '') {					
					$('.msg').text('Email Cannot be blank');
				} else {
					$('.msg').text('updating...');
					$.ajax({
						method: "POST",
						url: base_url+'/home/ChangeEmail',
						data: {email}
					}).done(function(data){
						if(data == 'success') {				
							window.location.href = base_url + "home";
						} else {
							$('.msg').text('failed');
						}
					});
				}
				
			});
        })
    </script>
</html>
