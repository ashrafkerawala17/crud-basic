<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!Doctype html>
<html>
	<head>
		<title>Beast</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel='stylesheet' href='assets/css/bootstrap.min.css'>
		<link rel='stylesheet' href='assets/css/login.css'>
	</head>
	<body>

		<section>
			<div class="container">
				<div class="formMain">
					<div class="formBox">
						<p class="loginText">Login</p>
						<div class="container-fluid">
							<label class="emailLabel" for="email">Email:</label>
							<input class="form-control emailInput" type="email" name="email" value='' placeholder='Enter Your E-mail' autocomplete='off'>
							<label class="passLabel" for="password">Password:</label>
							<input class="form-control passInput" type="password" name="password" value='' placeholder='Enter Your Passowrd' autocomplete='off'>
							<button class="form-control btn btn-primary submitButton" type="submit">Login</button>
							<a href="<?php echo base_url();?>signup">sign up</a>
							<p class="msg"></p>
						</div>
					</div>
				</div>
			</div>
		</section>

	</body>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {
			var base_url = "<?php echo base_url();?>";
			$('.submitButton').click(function(){
				email = $('.emailInput').val();
				pass = $('.passInput').val();
				if(email == '' && pass == '') {
					$('.msg').text('please fill all the details');
				} else {
					$('.msg').text('redirecting...');
					$.ajax({
						method: "POST",
						url: base_url+'/login/checkuser',
						data: {email,pass}
					}).done(function(data){
						if(data == 'success') {
							$('.msg').text('success');
							window.location.href = base_url +"home";
						} else {
							$('.msg').text('failed');
						}
					});
				}				
			});
		})
	</script>
</html>